s01: Basics of Relational Database (Activity)

Make a new file called solution.txt inside s01-a1 folder and answer the following questions based on the image:
1. List the books Authored by Marjorie Green.
	-au-id:
		213-46-8915
	-title_id
		BU1032
		BU2075
Answer:
	The Busy Executive’s Database Guide
	You Can Combat Computer Stress!
2. List the books Authored by Michael O'Leary.
	-au-id:
		267-41-2394
	-title_id
		BU1111
		TC7777 = unknown
Answer:
	Cooking with Computers
	
3. Write the author/s of "The Busy Executive’s Database Guide".
	-pub_id:
		1389
	-title_id:
		BU1032
	-Authors:
		-ID:
			409-56-7008
Answer:
		-Name:
			Bennet, Abraham
4. Identify the publisher of "But Is It User Friendly?".
	-pub_id:
		1389
	-title_id:
		PC1035
Answer:
	Algodata Infosystems
			
5. List the books published by Algodata Infosystems.
	-pub_id:
		1389
Answer:
	The Busy Executive's Database Guide
	Cooking with Computers
	Straight Talk About Computers
	But Is It User Friendly?
	Secrets of Silicon Valley
	Net Etiquette




